# HelperGenerator

Générer une réponse Helper

## Utilisation

Lancer la commande depuis votre terminal :
```sh
docker-compose up --build -d
```

Puis accéder à l'URL [http://localhost:666](http://localhost:666)