<!DOCTYPE html>
<html lang="fr-FR">
<head>
    <title>Générateur de réponse Helper</title>
    <meta content="Générateur de réponse pour ticket Helper" name="description" />
</head>
<body>
<h1>Générateur de réponse ticket Helper</h1>
<form method="post">
    <label>Action :
        <input name="action" type="text" value="<?php echo !empty($_POST['action']) ? htmlspecialchars($_POST['action']) : ''; ?>" placeholder="d'écouter de la musique"/>
    </label>
    <label>Service utilisé :
        <input name="service" type="text" value="<?php echo !empty($_POST['service']) ? htmlspecialchars($_POST['service']) : ''; ?>" placeholder="Spotify"/>
    </label>
    <input type="submit" value="Générer" />
</form>
<?php
if ($_POST) {
    echo '<div style="border-left: 5px solid gray; padding-left: 10px; margin: 20px 0 0 10px; max-width: 800px;">' .
    '<code>' .
    sprintf('Bonjour, Tout Sogetrel n\'a pas la possibilité %1$s.
    Certains de vos collaborateurs n\'ont pas cette possibilité. %2$s vous est-il indispensable dans le cadre de votre activité professionnelle ? La réponse est clairement négative. 
    Il n\'est pas convenable de déclarer un incident pour %2$s car ne pas disposez de cette application ne vous empêche pas de travailler sur les outils Sogetrel.
    A l\'instar de beaucoup de nos collaborateurs qui souhaite %1$s, je vous conseille de prendre un support personnel pour en faire. 
    Merci de votre compréhension. 
    Cordialement',
        !empty($_POST['action']) ? htmlspecialchars($_POST['action']) : '${action}',
        !empty($_POST['service']) ? htmlspecialchars($_POST['service']) : '${service}'
    ) .
    '</code>' .
    '</div>';
}
?>
</body>
</html>